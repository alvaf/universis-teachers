import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesDetailsTabsComponent } from './courses-details-tabs.component';

describe('CoursesDetailsTabsComponent', () => {
  let component: CoursesDetailsTabsComponent;
  let fixture: ComponentFixture<CoursesDetailsTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursesDetailsTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesDetailsTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
