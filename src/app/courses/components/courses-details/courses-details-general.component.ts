import { Component, OnInit, Input } from '@angular/core';
import { CoursesService } from '../../services/courses.service';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {BsDropdownModule} from 'ngx-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {ErrorService} from '../../../error/error.service';
import {LoadingService} from '../../../shared/services/loading.service';


@Component({
  selector: 'app-courses-details-general',
  templateUrl: './courses-details-general.component.html',
  styleUrls: ['./courses-details.component.scss']
})
export class CoursesDetailsGeneralComponent implements OnInit {
  public selectedClass: any;
  public isLoading = true;        // Only if data is loaded

  constructor(private _context: AngularDataContext,
              private translate: TranslateService,
              private  coursesService: CoursesService,
              private  errorService: ErrorService,
              private loadingService: LoadingService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {

    // show loading
    this.loadingService.showLoading();
    this.route.parent.params.subscribe(routeParams => {
      this.coursesService.getCourseClass(routeParams.course, routeParams.year, routeParams.period).then(courseClass => {
        // set selected course class
        this.selectedClass = courseClass;
        // hide loading
        this.loadingService.hideLoading();
        this.isLoading = false;
      }).catch(err => {
        // hide loading
        this.loadingService.hideLoading();
        return this.errorService.navigateToError(err);
      });
    });




  }



}
