import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructorCurrentCoursesComponent } from './instructor-current-courses.component';

describe('InstructorCurrentCoursesComponent', () => {
  let component: InstructorCurrentCoursesComponent;
  let fixture: ComponentFixture<InstructorCurrentCoursesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructorCurrentCoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorCurrentCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
