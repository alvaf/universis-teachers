import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {recognize} from '@angular/router/src/recognize';
import {CoursesService} from '../../../courses/services/courses.service';
import {ErrorService} from '../../../error/error.service';

@Component({
  selector: 'app-instructor-current-courses',
  templateUrl: './instructor-current-courses.component.html',
  styleUrls: ['./instructor-current-courses.component.scss']
})
export class InstructorCurrentCoursesComponent implements OnInit {

  public recentCourses: any = []; // Data
  public isLoading = true;        // Only if data is loaded

  constructor(private _context: AngularDataContext,
              private _coursesService: CoursesService,
              private _errorService: ErrorService) { }

  ngOnInit() {


    this._coursesService.getRecentCourses().then((res) => {
      this.recentCourses = res.value;
      this.isLoading = false;
    }).catch(err => {
      return this._errorService.navigateToError(err);
    });
  }

}
